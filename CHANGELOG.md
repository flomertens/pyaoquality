# Change Log

## 0.2.6 - 2024-11-22

- [Fix] Fix indexing issue that caused missing MS in AOQualityTimeStat.get_combined_stat()

## 0.2.5 - 2024-07-09

- [Changed] Update dependencies

## 0.2.4 - 2021-10-13

- [New] Add verbose argument to BaseAOQuality.from_ms_list()

## 0.2.1 - 2021-10-13

- [Fixed] Make BaseAOQuality picklable.

## 0.2 - 2021-08-07

- [New] Add the possibility to open multiple MS at once.

## 0.1 - 2021-08-06

- [New] Initial release.
